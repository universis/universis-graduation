# @universis/graduation

Student graduation components and services for client applications

# Usage

    npm i @universis/graduation

Update angular.json and include the following scripts which are required for `@universis/graduation`

    "styles": [
        "node_modules/gijgo/css/gijgo.min.css",
        ...
    ],
    "scripts": [
        "node_modules/jquery/dist/jquery.js",
        "node_modules/@universis/graduation/assets/gijgo/core.js",
        "node_modules/@universis/graduation/assets/gijgo/draggable.js",
        "node_modules/@universis/graduation/assets/gijgo/droppable.js",
        "node_modules/@universis/graduation/assets/gijgo/tree.js",
        ...
    ]

## Development

This project is a part of an angular cli project like `universis-students` or `universis`.

