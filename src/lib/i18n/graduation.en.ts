/* tslint:disable max-line-length */
export const en = {
  UniversisGraduationModule: {
    Certificates: 'Certificates',
    CertificateInfo: 'After the graduation ceremony your department will supply you with the following certificates',
    Degree: 'Degree',
    DegreePrerequisites: 'Degree prerequisites',
    Graduation: 'Graduation',
    GraduationRequestWizard: 'Graduation request wizard',
    GraduationRequestOverview: 'Graduation request overview',
    BackToDegree: 'Back to degree',
    GraduationDatesNotice: 'Graduation request period will be open from {{dateStart}} to {{dateEnd}}.',
    GraduationOpenEventNotice: {
      none: 'There are no events available.',
      one: '1 event available found.',
      other: '{{count}} event available found.'
    },
    WithoutGraduationRequestMessage: 'Graduation request period for {{semesterPeriod}} semester {{academicYear}} is open. You can use the graduation request wizard to submit the graduation request.',
    PotentialGraduationRequestMessage: 'Graduation request period for {{semesterPeriod}} semester {{academicYear}} is open. You can use the graduation request wizard to manage your graduation request.',
    OpenGraduationRequestPeriodMessage: 'Graduation request period is open. You can use the graduation request wizard to submit the graduation request.',
    ActiveGraduationRequestMessage: 'Graduation request period for {{semesterPeriod}} semester {{academicYear}} is pending inspection. You can use the graduation request wizard to check your graduation request progress.',
    CompletedGraduationRequestMessage: 'Graduation request period for {{semesterPeriod}} semester {{academicYear}} has been accepted. You can use the graduation request wizard to check your graduation request progress.',
    WithoutGraduationEventMessage: 'The Graduation event is not currently available in your department.',
    Semester: {
      title: 'Semester',
      summer: 'summer',
      winter: 'winter',
      summerPossessive: 'summer',
      winterPossessive: 'winter',
      caps: {
        title: 'SEMESTER',
        winter: 'WINTER',
        summer: 'SUMMER'
      }
    },
    GraduationRequestTitle: 'Graduation request',
    GraduationRequestCapitalTitle: 'GRADUATION REQUEST',
    SpecialRequest: {
      Title: 'Special request',
      Notice: 'If you need to make a special request, write your message at the field below.',
      InputPlaceholder: 'Your message to registrar',
      SelectEvent: 'Available graduation ceremonies'
    },
    GraduationRequest: {
      Title: 'Graduation request',
      DefaultRequestName: 'I request to submit graduation request for {{ academicPeriod }} semester {{academicYear}}.',
      RequirementsChecked: 'I agree that I will participate in graduation ceremony of {{academicPeriod}} semester',
      ParticipateInCeremony: 'I have checked that the requirements  are fulfilled',
      WillAnnounceDatesNotice:  'The graduation ceremony dates and location will be announced after the graduation request period',
      Request: 'Request',
      errors: {
        generic: 'There where an error during the request submission.',
        EUNQ: 'You have already submitted a graduation request for this graduation event.'
      }
    },
    RequirementsCheck: {
      Title: 'Requirements Check',
      Subtitle: 'To complete your Graduation Request, the necessary conditions must be fulfilled',
      StatusCheck: 'Status check',
      DegreeRequirements: 'Degree requirements',
      Status: {
        unavailable: 'The graduation requirements check has not started.',
        failed: 'Graduation requirements are not fulfilled.',
        pending: 'The graduation requirements check is ongoing.',
        completed: 'The graduation requirements check is completed successfully.'
      }
    },
    DocumentsSubmission: {
      Title: ' Documents submission',
      Subtitle: ' Follow the instructions in order to submit the required documents for your graduation.',
      SubmissionStatus: 'Submission status',
      SubmissionStatuses: {
        pending: 'Document submission for graduation is ongoing',
        completed: 'Document submission for graduation is completed',
        failed: 'Document submission for graduation is ongoing',
        unavailable: 'Document submission is not available'
      },
      AttachmentDeleteModal: {
        Title: 'Document delete',
        Body: 'Delete document of {{attachmentType}} type?',
        Notice: 'This action can not be undone.',
        Close: 'Close',
        Delete: 'Delete'
      },
      GraduationDocumentsToUpload: 'Graduation documents for upload',
      GraduationDocumentsPhysicals: 'Graduation documents to be delivered to the secretariat of the department',
      DownloadDocument: 'Download document',
      UploadDocument: 'Upload document',
      RemoveDocument: 'Remove document',
      ContactService: 'contact related service',
      Errors: {
        Download: 'There was an error during the file download',
        Remove: 'There was an error during the file removal',
        Upload: 'There was an error during the file upload'
      }
    },
    UserConsents: {
      Title: "Optional consents"
    },
    GraduationCeremony: {
      Title: 'Final Submission of Graduation Request',
      ParticipationStatus: 'Participation Status',
      GraduationCeremonyStatus: 'Graduation ceremony status',
      GraduationCertificates: 'Graduation certificates',
      ContactRegistrar: 'Contact registrar',
      DatesWillBeAnnouncedNotice: 'The Date and the Location of the graduation ceremony will be announced after the graduation request period.',
      MailWillBeSentNotice: 'You will receive relevant message at your email',
      MandatoryParticipation: 'Your participation at the graduation event is mandatory.'
    },
    ModalConfirm: {
      Submit: 'Submit',
      Close: 'Close',
      Title: 'Send Graduation Request',
      Body: 'You want to send your request for sending and check to the secretariat?'
    },
    MessagePrompt: 'Your message',
    Send: 'Send',
    Cancel: 'Cancel',
    Date: 'Date',
    EventName: 'Event Name',
    Time: 'Time',
    Location: 'Location',
    Url: 'Related URL',
    Download: 'Download',
    Previous:  'Previous',
    Next: 'Next',
    Completed: 'Request Completion',
    GraduationRules: 'Graduation Rules',
    GraduationApply: 'Apply for Graduation',
    ComplexRule: 'Complex Prerequisites',
    ComplexRules: 'The Study Program you attend has complex graduation rules',
    ComplexRulesSuccess: 'The degree requirements are met',
    ComplexRulesNoSuccess: 'Degree requirements are not met',
    LogicalOperators: {
      '&&': 'AND',
      '||': 'OR',
      '!': 'NOT'
    },
    ContactRegistrar: 'Contact Registrar',
    GraduationInfo: 'For more information about graduation rules you can check your studies guide or contact Registrar.',
    StudentInfo: 'Student Information',
    StudyGuide: 'STUDY GUIDE',
    Specialty : 'SPECIALTY',
    Prerequisites: 'Prerequisites',
    Progress: 'Progress',
    NoRulesFound: 'Graduation Rules have not been set.',
    CourseType: 'Course Type',
    AllTypeCourses: 'All Type Courses',
    Thesis: 'Thesis',
    Student: 'Student Information',
    Internship: 'Internship',
    Course: 'Prerequisite Courses',
    CourseArea: 'Courses Area',
    CourseCategory: 'Courses Category',
    CourseSector: 'Courses Sector',
    ProgramGroup: 'Courses Group',
    StatusLabel: 'Status of your request',
    NoAttachments: 'There are no attachments for upload'
  }
};
