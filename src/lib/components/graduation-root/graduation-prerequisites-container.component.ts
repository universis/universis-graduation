import {Component, OnInit} from '@angular/core';

@Component({
  selector: 'universis-graduation-prerequisites-container',
  template: `<universis-graduation-prerequisites [student]="'me'"></universis-graduation-prerequisites>`
})
export class GraduationPrerequisitesContainerComponent {

}
