import { Component, OnInit } from '@angular/core';
import {GraduationService} from '../../services/graduation-request/graduation.service';

@Component({
  selector: 'universis-graduation-root',
  templateUrl: './graduation-root.component.html',
  styleUrls: ['./graduation-root.component.scss']
})
export class GraduationRootComponent implements OnInit {

  graduationEventExist = true;

  constructor(private graduationService: GraduationService) { }

  async ngOnInit() {
    this.graduationService.getStudentRequestConfigurations().then ( (requestTypes) => {
      const graduationRequest =  requestTypes.find ( x => {
        return x.additionalType === 'GraduationRequestAction' && x.validationResult && x.validationResult.success === true;
      });
      if (!graduationRequest) {
        this.graduationEventExist = false;
      }
    });
  }

}
