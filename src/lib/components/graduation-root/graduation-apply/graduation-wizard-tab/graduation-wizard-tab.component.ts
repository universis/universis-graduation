import { Component, OnInit, Input } from '@angular/core';
import { BehaviorSubject, Subscription, Observable  } from 'rxjs';

@Component({
  template: ''
})
export class GraduationWizardTabComponent {

  /**
   * @property {string} alternateName A unique identifier for the graduation process step
   */
  protected alternateName: string;

  /**
   * @property {BehaviorSubject<any>} graduationRequestStatusObservable$
   * Handles the communication with the parent component
   */
  @Input() graduationRequestStatusObservable$: BehaviorSubject<any>;

  /**
   * @property {Subscription} graduationRequestStatusSubscription
   * The overall graduation request status
   */
  graduationRequestStatusSubscription: Subscription;

  /**
   * @property {currentStep} current step with available data
   */
  @Input() currentStep: any;

}
