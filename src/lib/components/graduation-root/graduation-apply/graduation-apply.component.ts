import {Component, OnInit} from '@angular/core';
import {ErrorService, LoadingService} from '@universis/common';
import {Router} from '@angular/router';
import {TranslateService} from '@ngx-translate/core';
import {GraduationService} from '../../../services/graduation-request/graduation.service';
import {AppEventService} from '@universis/common';
import {Subscription} from 'rxjs';

@Component({
  selector: 'universis-graduation-apply',
  templateUrl: './graduation-apply.component.html'
})
export class GraduationApplyComponent implements OnInit {

  /**
   *
   * A flag that indicates wether the current date is a graduation period for the user's department
   *
   */
  public inGraduationPeriod: boolean;

  /**
   *
   * Information regarding the graduation request
   *
   */
  public requestPeriodExists = false;

  public graduationEvents: any[];
  public currentGraduationRequest: any;
  public canAccessToGraduationRequest: any;
  public student: any;
  public requestThirdStep: any;
  public requestFourthStep: any;
  public isLoading = true;

  constructor(
    private _router: Router,
    private _errorService: ErrorService,
    private _translateService: TranslateService,
    private _loadingService: LoadingService,
    private graduationService: GraduationService,
    private _appEventService: AppEventService
  ) {
  }

  async ngOnInit() {
    try {

      this._loadingService.showLoading();
      // check if the student has access to GraduationRequests
      this.graduationService.getStudentRequestConfigurations().then((requestTypes) => {
        const graduationRequestAction = requestTypes.find(x => {
          return x.additionalType === 'GraduationRequestAction';
        });
        if (!graduationRequestAction) {
          this._errorService.showError(
            {
              error: 404
            });
        }
      });
      this.canAccessToGraduationRequest = await this.graduationService.checkAccessGraduationRequest('me');

      if (this.canAccessToGraduationRequest.data) {
        if (this.canAccessToGraduationRequest.data.request) {
          // get available Graduation Request (first only)
          this.currentGraduationRequest = this.canAccessToGraduationRequest.data.request;
        }

        if (this.canAccessToGraduationRequest.data.events) {
          // get available Graduation Events
          this.graduationEvents = this.canAccessToGraduationRequest.data.events;
        }
      }

      this.student = await this.graduationService.getStudent('me');

      this._appEventService.change.next({
        model: 'GraduationData',
        target: {
          events: this.graduationEvents,
          request: this.currentGraduationRequest,
          student: this.student
        }
      });

      // get preview request
      if (this.currentGraduationRequest) {
        if (this.currentGraduationRequest.actionStatus.alternateName === 'ActiveActionStatus' ||
          this.currentGraduationRequest.actionStatus.alternateName === 'CompletedActionStatus') {

          const thirdStep = {
            index: 2,
            alternateName: 'graduationDocumentsSubmission',
            title: 'DocumentsSubmission',
            required: true,
            enabled: true,
            status: 'completed'
          };

          this.requestThirdStep = await this.graduationService.getStepData(thirdStep,
            this.currentGraduationRequest,
            this.graduationEvents,
            this.student);

          const fourthStep = {
            index: 3,
            alternateName: 'graduationCeremony',
            title: 'GraduationCeremony',
            required: true,
            enabled: true,
            status: 'completed'
          };

          this.requestFourthStep = await this.graduationService.getStepData(fourthStep,
            this.currentGraduationRequest,
            this.graduationEvents,
            this.student);
        }
      }

    } catch (err) {
      console.error(err);
    } finally {
      this.isLoading = false;
      this._loadingService.hideLoading();
    }
  }

  /**
   *
   * Forms the title of the message box
   *
   */
  getTitle(): string {
    if (this.currentGraduationRequest) {
      const academicPeriod = this._translateService.instant(
        `UniversisGraduationModule.Semester.caps.${this.currentGraduationRequest.graduationEvent.graduationPeriod.alternateName}`
      );
      const semester = this._translateService.instant(`UniversisGraduationModule.Semester.caps.title`);

      return `${academicPeriod} ${semester} ${this.currentGraduationRequest.graduationEvent.graduationYear.alternateName}`;
    } else if (this.graduationEvents && this.graduationEvents.length > 0) {
      return this._translateService.instant(`UniversisGraduationModule.OpenGraduationRequestPeriodMessage`);
    } else {
      return this._translateService.instant('UniversisGraduationModule.GraduationRequestCapitalTitle');
    }
  }

  /**
   *
   * Forms the icon of the message box
   *
   * @param graduationRequestData The status of the graduation request
   *
   */
  getIcon(): string {
    if (this.currentGraduationRequest) {
      return 'far fa-check-circle';
    } else if (this.graduationEvents && this.graduationEvents.length > 0) {
      return 'far fa-check-circle';
    } else {
      return 'far fa-check-circle';
    }
  }

  /**
   *
   * Calculates the message of the message box
   *
   * @param graduationRequestData The status of the graduation request
   *
   */
  getMessage(): string {
    const request = this.currentGraduationRequest;

    if (request) {
      const graduationEvent = request.graduationEvent;

      const graduationPeriod = graduationEvent.graduationPeriod.alternateName;
      const graduationYear = graduationEvent.graduationYear.alternateName;

      const status = request.actionStatus.alternateName;

      if (status === 'PotentialActionStatus') {
        return this._translateService.instant('UniversisGraduationModule.PotentialGraduationRequestMessage', {
          semesterPeriod: this._translateService.instant(`UniversisGraduationModule.Semester.${graduationPeriod}`),
          academicYear: graduationYear
        });
      } else if (status === 'ActiveActionStatus') {
        return this._translateService.instant('UniversisGraduationModule.ActiveGraduationRequestMessage', {
          semesterPeriod: this._translateService.instant(`UniversisGraduationModule.Semester.${graduationPeriod}`),
          academicYear: graduationYear
        });
      } else if (status === 'CompletedActionStatus') {
        return this._translateService.instant('UniversisGraduationModule.CompletedGraduationRequestMessage', {
          semesterPeriod: this._translateService.instant(`UniversisGraduationModule.Semester.${graduationPeriod}`),
          academicYear: graduationYear
        });
      }
    } else if (this.graduationEvents && this.graduationEvents.length > 0) {
      let message = '';

      const event = this.graduationEvents[0];
      const graduationPeriod = event.graduationPeriod.alternateName;
      const graduationYear = event.graduationYear.alternateName;

      message += this._translateService.instant('UniversisGraduationModule.WithoutGraduationRequestMessage', {
        semesterPeriod: this._translateService.instant(`UniversisGraduationModule.Semester.${graduationPeriod}`),
        academicYear: graduationYear});

      return message;
    } else {
      return this._translateService.instant('UniversisGraduationModule.WithoutGraduationEventMessage');
    }
  }

  /**
   *
   * Change the location to the graduation request
   *
   */
  public navigateToGraduationRequest() {
    this._router.navigate(['graduation/request']);
  }

}
