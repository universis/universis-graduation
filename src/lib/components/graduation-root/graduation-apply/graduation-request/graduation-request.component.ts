import {
  Component,
  OnDestroy,
  ViewContainerRef,
  ComponentFactoryResolver,
  ViewChild,
  AfterViewInit,
  TemplateRef
} from '@angular/core';
import { Subscription, BehaviorSubject } from 'rxjs';
import {AppEventService, ErrorService, LoadingService, ModalService} from '@universis/common';
import { GraduationService } from '../../../../services/graduation-request/graduation.service';
import {Router} from '@angular/router';
import {GraduationRequestStep} from '../../../../graduation-request-step';
import {GraduationRequestFormComponent} from './steps/graduation-request-form/graduation-request-form.component';
import {
  GraduationRequirementsCheckComponent
} from './steps/graduation-requirements-check/graduation-requirements-check.component';
import {
  GraduationDocumentSubmissionComponent
} from './steps/graduation-document-submission/graduation-document-submission.component';
import {GraduationCeremonyComponent} from './steps/graduation-ceremony/graduation-ceremony.component';
import {AngularDataContext} from "@themost/angular";

@Component({
  selector: 'universis-graduation-request',
  templateUrl: './graduation-request.component.html',
  styles: [`

    .fa-ban:before {
      background-color: #d0d0d0;
      padding: 2px;
      border-radius: 20px;
    }

  `]
})
export class GraduationRequestComponent implements AfterViewInit, OnDestroy {
  // This component is based on this example
  // https://angular.io/guide/dynamic-component-loader

  @ViewChild('GraduationRequestWizardDirective', {read: ViewContainerRef}) GraduationRequestWizard;

  /**
   * The observable that holds the graduation status
   */
  public graduationStatusAsObservable$: BehaviorSubject<any>;

  /**
   * The form submission for the graduation form
   */
  private currentStepOutput: Subscription;
  private changeSubscription: Subscription;

  /**
   * The list of the wizard steps
   */
  public wizardSteps: any;

  /**
   * The current active step object
   */
  public activeStep;

  // data
  public graduationEvents: any[];
  public selectedEvent: any[];
  public currentGraduationRequest: any;
  public student: any;

  public isLoading = true;

  modalRef;
  @ViewChild('ConfirmModalTemplate') modalTemplate: TemplateRef<any>;


  constructor(
    private graduationService: GraduationService,
    private _componentFactoryResolver: ComponentFactoryResolver,
    private _errorService: ErrorService,
    private _modalService: ModalService,
    private _loadingService: LoadingService,
    private _router: Router,
    private _appEventService: AppEventService,
    protected _context: AngularDataContext
  ) { }


  async ngAfterViewInit() {
    try {
      this._loadingService.showLoading();
      this.graduationService.getStudentRequestConfigurations().then ( (requestTypes) => {
        const graduationRequest =  requestTypes.find ( x => {
          return x.additionalType === 'GraduationRequestAction';
        });
        if (!graduationRequest) {
          this._errorService.showError(
            {
              error: 404
            });
        }
      });

      // the data is filled by the apply component
      this.changeSubscription = this._appEventService.change.subscribe((event: { model: string; target: any }) => {
        if (event && event.model === 'GraduationData') {
          if (event.target != null) {
            this.graduationEvents = event.target.events;
            this.currentGraduationRequest =  event.target.request;
            this.student = event.target.student;
          }
        }
      });

      // initialize subscription for changing current step data
      this.graduationStatusAsObservable$ = new BehaviorSubject(undefined);

      // go to apply page when refreshing or when you use the spesific url
      if (!this.graduationEvents && !this.currentGraduationRequest) {
        await this._router.navigate(['/graduation/apply']);
      }

      this.wizardSteps =
        [
          {
            index: 0,
            alternateName: 'graduationRequest',
            title: 'GraduationRequest',
            required: true,
            enabled: true,
            status: 'completed'
          },
          {
            index: 1,
            alternateName: 'graduationRequirementsCheck',
            title: 'RequirementsCheck',
            required: false,
            enabled: true,
            status: 'completed'
          },
          {
            index: 2,
            alternateName: 'graduationDocumentsSubmission',
            title: 'DocumentsSubmission',
            required: true,
            enabled: true,
            status: 'completed'
          },
          {
            index: 3,
            alternateName: 'graduationCeremony',
            title: 'GraduationCeremony',
            required: true,
            enabled: true,
            status: 'completed'
          }
        ];

      await this.changeStep(0, 'front');
    } catch (err) {
      console.error(err);
    } finally {
      this._loadingService.hideLoading();
      this.isLoading = false;
    }
  }

  ngOnDestroy() {
    if (this.currentStepOutput) {
      this.currentStepOutput.unsubscribe();
    }
    if (this.changeSubscription) {
      this.changeSubscription.unsubscribe();
    }
  }

  async getStepData(menuStep) {
    // get step data
    const stepData = await this.graduationService.getStepData(
      menuStep,
      this.currentGraduationRequest,
      this.graduationEvents,
      this.student
    );
    return menuStep;
  }

  async changeStep(indexStep, direction) {
    const stepData = await this.getStepData(this.wizardSteps[indexStep] );
    if (stepData) {
      await this.loadComponent(stepData, direction);
    }
  }

  /**
   *
   * Shows the component (step) at the wizard
   *
   * @param index The index inside the steps array to show
   * direction : 'front' , 'back'
   *
   */
  async loadComponent(step: any, direction: string) {
    if (step.status === 'unavailable') {
      if (direction === 'front') {
        await this.changeStep(step.index + 1, direction);
      } else {
        await this.changeStep(step.index - 1, direction);
      }
      return;
    }

    this._loadingService.showLoading();
    this.isLoading = true;

    const component = this.graduationService.getComponent(step.alternateName).component;
    this.activeStep = step;
    const componentFactory = this._componentFactoryResolver.resolveComponentFactory(component);
    this.GraduationRequestWizard.clear();
    const currentComponent = this.GraduationRequestWizard.createComponent(componentFactory);
    currentComponent.instance.currentStep = step;
    currentComponent.instance.graduationRequestStatusObservable$ = this.graduationStatusAsObservable$;
    currentComponent.changeDetectorRef.detectChanges();

    // special configuration for components
    if (this.activeStep.alternateName === 'graduationRequest') {
      this.currentStepOutput = currentComponent.instance.currentStepOutput.subscribe((value) => this.submitGraduationForm(value));
    }

    if (this.activeStep.alternateName === 'graduationDocumentsSubmission') {
      this.currentStepOutput = currentComponent.instance.outputAction.subscribe(
        async (event) => this.handleDocumentsSubmissionStepOutput(event)
      );
    }
    this._loadingService.hideLoading();
    this.isLoading = false;
  }

  /**
   *
   * Submits the graduation form
   *
   * @param value The graduation request form submission
   *
   */
  async submitGraduationForm(data) {
    this.selectedEvent = data.graduationEvent;
    try {
      this.isLoading = true;
      this._loadingService.showLoading();

      const graduationRequest = await this.graduationService.setGraduationRequest(data.graduationEvent.id, data.form);
      // reload request to get graduation event attributes
      this.currentGraduationRequest = await this._context.model('GraduationRequestActions')
        .where('id').equal(graduationRequest.id)
        .expand('graduationEvent($expand=graduationPeriod,graduationYear,location,attachmentTypes($expand=attachmentType),reportTemplates),attachments($attachmentType)')
        .getItem();
      if (Array.isArray(data.form.userConsents) && data.form.userConsents.length > 0) {
        await this.graduationService.setGraduationUserConsents(data.form.userConsents);
      }

      // refresh step data
      this.activeStep = await this.getStepData(this.wizardSteps[this.activeStep.index] );
      this.graduationStatusAsObservable$.next(this.activeStep);
    } catch (err) {
      this.activeStep.data.errors = {
        code: err.error && err.error.code ? err.error.code : err,
        message: err.error && err.error.message ? err.error.message : err,
        innerMessage: err.error && err.error.innerMessage ? err.error.innerMessage : err
      };
      this.graduationStatusAsObservable$.next(this.activeStep);
    } finally {
      this._loadingService.hideLoading();
      this.isLoading = false;
    }
  }

  /**
   *
   * Handles the document submission events
   *
   * @param event The data passed by the documents submission step
   *
   */
  async handleDocumentsSubmissionStepOutput(event) {
    this._loadingService.showLoading();
    this.isLoading = true;
    try {
      if (event.action === 'upload') {
        await this.graduationService.uploadGraduationRequestAttachment(event.data);
      } else if (event.action === 'remove') {
        await this.graduationService.removeGraduationRequestAttachment(event.data);
      } else if (event.action === 'download') {
        await this.graduationService.downloadGraduationRequestAttachment(event.data);
      }
      const canAccessToGraduationRequest = await this.graduationService.checkAccessGraduationRequest('me');
      if (canAccessToGraduationRequest.data && canAccessToGraduationRequest.data.request) {
        this.currentGraduationRequest = canAccessToGraduationRequest.data.request;
      }
      // refresh step data
      this.activeStep = await this.getStepData(this.wizardSteps[this.activeStep.index] );


      this.graduationStatusAsObservable$.next(this.activeStep);
    } catch (err) {
      this.activeStep.data.errors = {
        code: err.error && err.error.code ? err.error.code : err,
        message: err.error && err.error.message ? err.error.message : err,
        innerMessage: err.error && err.error.innerMessage ? err.error.innerMessage : err
      };
      this.graduationStatusAsObservable$.next(this.activeStep);
    } finally {
      this._loadingService.hideLoading();
      this.isLoading = false;
    }
  }

  openConfirmModal() {
    this.modalRef = this._modalService.openModal(this.modalTemplate);
  }

  closeConfirmModal() {
    this.modalRef.hide();
  }

  async completeRequest(data) {
    this._loadingService.showLoading();
    this.isLoading = true;
    try {
      await this.graduationService.setGraduationRequestActiveStatus(data);
      this._router.navigateByUrl('graduation/apply');
    } catch (err) {
      this.activeStep.data.errors = {
        code: err.error && err.error.code ? err.error.code : err,
        message: err.error && err.error.message ? err.error.message : err,
        innerMessage: err.error && err.error.innerMessage ? err.error.innerMessage : err
      };
      this.graduationStatusAsObservable$.next(this.activeStep);
    } finally {
      this.closeConfirmModal();
      this._loadingService.hideLoading();
      this.isLoading = false;
    }
  }

}
