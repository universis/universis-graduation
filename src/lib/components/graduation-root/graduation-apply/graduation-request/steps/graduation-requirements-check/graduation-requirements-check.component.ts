import { Component, OnInit, OnDestroy, Input } from '@angular/core';
import { TranslateService } from '@ngx-translate/core';
import { GraduationWizardTabComponent } from '../../../graduation-wizard-tab/graduation-wizard-tab.component';

@Component({
  selector: 'universis-graduation-requirements-check',
  templateUrl: './graduation-requirements-check.component.html'
})
export class GraduationRequirementsCheckComponent extends GraduationWizardTabComponent implements OnInit, OnDestroy {

  public graduationInfo: any;
  public studentGuide: any;
  public studentSpecialty: any;
  /**
   * The graduation event
   */

  /**
   *
   * A message for the status of the overall process
   *
   */
  public statusMessage: string | undefined;


  constructor(
    private _translateService: TranslateService
  ) {
    super();
   }


  ngOnInit() {
    if (this.graduationRequestStatusObservable$) {
      this.graduationRequestStatusSubscription = this.graduationRequestStatusObservable$.subscribe((step) => {
        this.fillData();
        if (step) {
          this.currentStep = step;
        }
      });
    } else {
      this.fillData();
    }
  }

  fillData() {
    if (this.currentStep && this.currentStep.data) {
      this.graduationInfo = this.currentStep.data.graduationInfo;
      this.studentGuide = this.currentStep.data.student.studyProgram.name;
      this.studentSpecialty = this.currentStep.data.student.specialty;

      if (this.currentStep.status) {
        this.statusMessage = this._translateService.instant(
          `UniversisGraduationModule.RequirementsCheck.Status.${this.currentStep.status}`
        );
      } else {
        this.statusMessage = undefined;
      }
    }
  }

  ngOnDestroy(): void {
    if (this.graduationRequestStatusSubscription) {
      this.graduationRequestStatusSubscription.unsubscribe();
    }
  }
}
