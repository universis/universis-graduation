import {CUSTOM_ELEMENTS_SCHEMA, ModuleWithProviders, NgModule, OnInit, Optional, SkipSelf} from '@angular/core';
import { CommonModule } from '@angular/common';
import {TranslateModule, TranslateService} from '@ngx-translate/core';
import { SharedModule} from '@universis/common';
import { FormsModule } from '@angular/forms';

import {GRADUATION_LOCALES} from './i18n';
import {GraduationPrerequisitesComponent} from './components/graduation-root/graduation-rules/graduation-prerequisites/graduation-prerequisites.component';
import { GraduationTreeComponent } from './components/graduation-root/graduation-rules/graduation-tree/graduation-tree.component';
import {GraduationProgressComponent} from './components/graduation-root/graduation-rules/graduation-progress/graduation-progress.component';
import {GraduationService} from './services/graduation-request/graduation.service';
import {NgPipesModule} from 'ngx-pipes';

@NgModule({
  imports: [
    CommonModule,
    TranslateModule,
    SharedModule,
    FormsModule,
    NgPipesModule

  ],
  declarations: [
    GraduationPrerequisitesComponent,
    GraduationProgressComponent,
    GraduationTreeComponent
  ],
  exports: [
    GraduationPrerequisitesComponent,
    GraduationProgressComponent,
    GraduationTreeComponent
  ],
  schemas: [
    CUSTOM_ELEMENTS_SCHEMA
  ]
})
export class GraduationSharedModule implements OnInit {
  constructor(@Optional() @SkipSelf() parentModule: GraduationSharedModule,
              private _translateService: TranslateService) {
    if (parentModule) {
      // throw new Error(
      //    'StudentsSharedModule is already loaded. Import it in the AppModule only');
    }
    this.ngOnInit().catch(err => {
      console.error('An error occurred while loading students shared module');
      console.error(err);
    });
  }

  static forRoot(): ModuleWithProviders<GraduationSharedModule> {
    return {
      ngModule: GraduationSharedModule,
      providers: [
        GraduationService
      ]
    };
  }

  async ngOnInit() {
    Object.keys(GRADUATION_LOCALES).forEach( language => {
      if (GRADUATION_LOCALES.hasOwnProperty(language)) {
        this._translateService.setTranslation(language, GRADUATION_LOCALES[language], true);
      }
    });
  }

}
